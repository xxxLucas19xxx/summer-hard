
CREATE database trabalhofinal;
USE trabalhofinal;
CREATE TABLE hf_usuario(
	ID INT AUTO_INCREMENT PRIMARY KEY,
    nome CHAR(30),
    senha CHAR(12),
    email VARCHAR(100)
	);
insert into hf_usuario(nome) values ('sksd');    
CREATE TABLE hf_carona(
	ID INT AUTO_INCREMENT PRIMARY KEY,
	repeticao VARCHAR(30),
    data DATE,
    horario TIME,
    espaco INT(2),
    usuario_ID INT,
	FOREIGN KEY(usuario_ID)
    REFERENCES hf_usuario(ID)
);
alter table hf_carona modify repeticao varchar(30);

CREATE TABLE hf_pontos(
	ID INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(20)
);

CREATE TABLE hf_querer(
	usuario_ID INT,
	FOREIGN KEY(usuario_ID)
    REFERENCES hf_usuario(ID),
    carona_ID INT,
    FOREIGN KEY(carona_ID)
    REFERENCES hf_carona(ID),
    primary key(usuario_ID,carona_ID)
    );
    
CREATE TABLE hf_caminho(
	carona_ID INT,
	FOREIGN KEY(carona_ID)
	REFERENCES hf_carona(ID),
	pontos_ID int,
	FOREIGN KEY(pontos_ID)
	REFERENCES hf_pontos(ID),
    primary key(carona_ID,pontos_ID)
);

SELECT hf_pontos.nome,count(*)
from hf_caminho join hf_carona on hf_caminho.carona_ID = hf_carona.ID
join hf_pontos on hf_pontos.ID = hf_caminho.pontos_ID
group by hf_pontos.ID;