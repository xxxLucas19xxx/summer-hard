<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfQuerer */

$this->title = 'Atualizar Lista: ' . $model->usuario_ID;
$this->params['breadcrumbs'][] = ['label' => 'Hf Querers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario_ID, 'url' => ['view', 'usuario_ID' => $model->usuario_ID, 'carona_ID' => $model->carona_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hf-querer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
