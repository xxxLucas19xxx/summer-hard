<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HfQuererSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Caronas Ativas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-querer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Procurar carona', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'usuario_ID',
            'carona_ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

        <p>Aqui você pega caronas, preencha o seu ID e o ID da carona que quiser pegar e pronto.</p>

</div>
