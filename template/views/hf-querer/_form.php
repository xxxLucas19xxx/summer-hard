<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HfQuerer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hf-querer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'usuario_ID')->textInput() ?>

    <?= $form->field($model, 'carona_ID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
