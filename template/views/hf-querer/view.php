<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HfQuerer */

$this->title = $model->usuario_ID;
$this->params['breadcrumbs'][] = ['label' => 'Hf Querers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hf-querer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'usuario_ID' => $model->usuario_ID, 'carona_ID' => $model->carona_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'usuario_ID' => $model->usuario_ID, 'carona_ID' => $model->carona_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Todo dia tem uma merda dessas! Vai mesmo me apagar?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'usuario_ID',
            'carona_ID',
        ],
    ]) ?>

</div>
