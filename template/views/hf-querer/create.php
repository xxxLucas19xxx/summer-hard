<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfQuerer */

$this->title = 'Nova busca';
$this->params['breadcrumbs'][] = ['label' => 'Hf Querers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-querer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
