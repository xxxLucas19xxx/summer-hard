<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatorios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
 
</div>

<?= Html::a('Quantidade de caronas por ponto', ['relatorio1'], ['class' => 'btn btn-success']) ?>
<br><br>
<?= Html::a('Quantidade de caronas por horário', ['relatorio2'], ['class' => 'btn btn-success']) ?>
<br><br>
<?= Html::a('Quantidade de caronas por espaço', ['relatorio3'], ['class' => 'btn btn-success']) ?>
<br><br>
<?= Html::a('Quantidade de pessoas por carona', ['relatorio4'], ['class' => 'btn btn-success']) ?>
<br><br>
<?= Html::a('Média de caronas ofertadas por usuario', ['relatorio5'], ['class' => 'btn btn-success']) ?>