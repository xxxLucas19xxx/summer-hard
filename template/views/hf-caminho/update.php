<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfCaminho */

$this->title = 'Atualizar rota: ' . $model->carona_ID;
$this->params['breadcrumbs'][] = ['label' => 'Hf Caminhos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->carona_ID, 'url' => ['view', 'carona_ID' => $model->carona_ID, 'pontos_ID' => $model->pontos_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hf-caminho-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
