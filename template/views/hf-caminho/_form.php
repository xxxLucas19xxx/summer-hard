<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HfCaminho */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hf-caminho-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'carona_ID')->textInput() ?>

    <?= $form->field($model, 'pontos_ID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
