<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HfCaminhoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Caminhos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-caminho-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nova Rota', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'carona_ID',
            'pontos_ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

<p>Aqui você define o caminho por onde sua carona passará, selecione o id da caroa e o id do ponto (repita isso para cada ponto que irá passar).</p>

</div>
