<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HfCaminho */

$this->title = $model->carona_ID;
$this->params['breadcrumbs'][] = ['label' => 'Caminhos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hf-caminho-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'carona_ID' => $model->carona_ID, 'pontos_ID' => $model->pontos_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'carona_ID' => $model->carona_ID, 'pontos_ID' => $model->pontos_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'você quer apaga isso desgraça?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'carona_ID',
            'pontos_ID',
        ],
    ]) ?>

</div>
