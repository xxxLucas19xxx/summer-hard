<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HfCaminhoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hf-caminho-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'carona_ID') ?>

    <?= $form->field($model, 'pontos_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('procurar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('resetar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
