<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfCaminho */

$this->title = 'Nova Rota';
$this->params['breadcrumbs'][] = ['label' => 'Hf Caminhos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-caminho-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
