<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HfCaronaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Caronas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-carona-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nova carona', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'repeticao',
            'data',
            'horario',
            'espaco',
            //'usuario_ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

        <p>Aqui você pode oferecer caronas para as pessoas, definir quantas vezes vai repeti-la e quantos lugares estão disponíveis. <p>

</div>
