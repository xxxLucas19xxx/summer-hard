<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HfCaronaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hf-carona-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'repeticao') ?>

    <?= $form->field($model, 'data') ?>

    <?= $form->field($model, 'horario') ?>

    <?= $form->field($model, 'espaco') ?>

    <?php // echo $form->field($model, 'usuario_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('procurar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
