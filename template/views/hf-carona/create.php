<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfCarona */

$this->title = 'Nova carona';
$this->params['breadcrumbs'][] = ['label' => 'Hf Caronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-carona-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
