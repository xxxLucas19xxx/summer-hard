<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HfCarona */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hf-carona-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'repeticao')->textInput() ?>

    <?= $form->field($model, 'data')->textInput(['type'=>'date']) ?>

    <?= $form->field($model, 'horario')->textInput(['type'=>'time']) ?>

    <?= $form->field($model, 'espaco')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'usuario_ID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
