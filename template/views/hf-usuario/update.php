<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfUsuario */

$this->title = 'Atualizar Usuario: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Hf Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hf-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
