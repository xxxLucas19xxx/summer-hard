<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfUsuario */

$this->title = 'Criar novo usuário ';
$this->params['breadcrumbs'][] = ['label' => 'Hf Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-usuario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
