<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HfPontosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pontos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-pontos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar Pontos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'nome',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

        <p>Aqui você pode cadastrar novos pontos.</p>

</div>
