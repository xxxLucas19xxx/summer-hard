<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfPontos */

$this->title = 'Atualizar os pontos: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Hf Pontos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hf-pontos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
