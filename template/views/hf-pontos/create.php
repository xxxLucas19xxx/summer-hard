<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HfPontos */

$this->title = 'Novos Pontos';
$this->params['breadcrumbs'][] = ['label' => 'Hf Pontos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hf-pontos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
