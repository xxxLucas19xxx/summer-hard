<?php

namespace app\controllers;

use Yii;
use app\models\HfQuerer;
use app\models\HfQuererSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HfQuererController implements the CRUD actions for HfQuerer model.
 */
class HfQuererController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HfQuerer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HfQuererSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HfQuerer model.
     * @param integer $usuario_ID
     * @param integer $carona_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($usuario_ID, $carona_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($usuario_ID, $carona_ID),
        ]);
    }

    /**
     * Creates a new HfQuerer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HfQuerer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'usuario_ID' => $model->usuario_ID, 'carona_ID' => $model->carona_ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HfQuerer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $usuario_ID
     * @param integer $carona_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($usuario_ID, $carona_ID)
    {
        $model = $this->findModel($usuario_ID, $carona_ID);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'usuario_ID' => $model->usuario_ID, 'carona_ID' => $model->carona_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HfQuerer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $usuario_ID
     * @param integer $carona_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($usuario_ID, $carona_ID)
    {
        $this->findModel($usuario_ID, $carona_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HfQuerer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $usuario_ID
     * @param integer $carona_ID
     * @return HfQuerer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($usuario_ID, $carona_ID)
    {
        if (($model = HfQuerer::findOne(['usuario_ID' => $usuario_ID, 'carona_ID' => $carona_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
