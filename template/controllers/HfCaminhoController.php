<?php

namespace app\controllers;

use Yii;
use app\models\HfCaminho;
use app\models\HfCaminhoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HfCaminhoController implements the CRUD actions for HfCaminho model.
 */
class HfCaminhoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HfCaminho models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HfCaminhoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HfCaminho model.
     * @param integer $carona_ID
     * @param integer $pontos_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($carona_ID, $pontos_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($carona_ID, $pontos_ID),
        ]);
    }

    /**
     * Creates a new HfCaminho model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HfCaminho();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'carona_ID' => $model->carona_ID, 'pontos_ID' => $model->pontos_ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HfCaminho model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $carona_ID
     * @param integer $pontos_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($carona_ID, $pontos_ID)
    {
        $model = $this->findModel($carona_ID, $pontos_ID);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'carona_ID' => $model->carona_ID, 'pontos_ID' => $model->pontos_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HfCaminho model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $carona_ID
     * @param integer $pontos_ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($carona_ID, $pontos_ID)
    {
        $this->findModel($carona_ID, $pontos_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HfCaminho model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $carona_ID
     * @param integer $pontos_ID
     * @return HfCaminho the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($carona_ID, $pontos_ID)
    {
        if (($model = HfCaminho::findOne(['carona_ID' => $carona_ID, 'pontos_ID' => $pontos_ID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
