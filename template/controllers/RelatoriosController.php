<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;
 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }

   public function actionRelatorio1()
   {
       $Relatorio = new SqlDataProvider([
        'sql' => 'SELECT hf_pontos.nome, COUNT(*) AS Número_de_pontos
        FROM hf_caminho JOIN hf_carona ON hf_caminho.carona_ID = hf_carona.ID
        JOIN hf_pontos ON hf_pontos.ID = hf_caminho.pontos_ID
        GROUP BY hf_pontos.ID',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $Relatorio]);
   }

   public function actionRelatorio2()
   {
       $Relatorio = new SqlDataProvider([
        'sql' => 'SELECT hf_carona.horario, COUNT(*) AS Número_de_caronas
        from hf_carona
        GROUP BY hf_carona.horario',
            ]
        );
        
        return $this->render('relatorio2', ['resultado' => $Relatorio]);
   }

   public function actionRelatorio3()
   {
       $Relatorio = new SqlDataProvider([
        'sql' => 'SELECT hf_carona.espaco, COUNT(*) AS Número_de_caronas
        FROM hf_carona
        GROUP BY hf_carona.espaco',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $Relatorio]);
   }

   public function actionRelatorio4()
   {
       $Relatorio = new SqlDataProvider([
        'sql' => 'SELECT hf_querer.carona_id, COUNT(*) AS Número_de_pessoas
        FROM hf_querer
        GROUP BY hf_querer.carona_id',
            ]
        );
        
        return $this->render('relatorio4', ['resultado' => $Relatorio]);
   }

   public function actionRelatorio5()
   {
       $Relatorio = new SqlDataProvider([
        'sql' => 'SELECT avg(qnt) AS Média FROM (
            SELECT count(*) AS qnt
            FROM hf_carona
            GROUP BY hf_carona.usuario_id) x',
            ]
        );
        
        return $this->render('relatorio5', ['resultado' => $Relatorio]);
   }

}
?>