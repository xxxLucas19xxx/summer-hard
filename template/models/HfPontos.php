<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hf_pontos".
 *
 * @property int $ID
 * @property string $nome
 *
 * @property HfCaminho[] $hfCaminhos
 * @property HfCarona[] $caronas
 */
class HfPontos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hf_pontos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Número do ponto',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHfCaminhos()
    {
        return $this->hasMany(HfCaminho::className(), ['pontos_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaronas()
    {
        return $this->hasMany(HfCarona::className(), ['ID' => 'carona_ID'])->viaTable('hf_caminho', ['pontos_ID' => 'ID']);
    }
}
