<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hf_carona".
 *
 * @property int $ID
 * @property string $repeticao
 * @property string $data
 * @property string $horario
 * @property int $espaco
 * @property int $usuario_ID
 *
 * @property HfCaminho[] $hfCaminhos
 * @property HfPontos[] $pontos
 * @property HfUsuario $usuario
 * @property HfQuerer[] $hfQuerers
 * @property HfUsuario[] $usuarios
 */
class HfCarona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hf_carona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'horario'], 'safe'],
            [['espaco', 'usuario_ID'], 'integer'],
            [['repeticao'], 'string', 'max' => 30],
            [['usuario_ID'], 'exist', 'skipOnError' => true, 'targetClass' => HfUsuario::className(), 'targetAttribute' => ['usuario_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'repeticao' => 'Repeticao',
            'data' => 'Data',
            'horario' => 'Horario',
            'espaco' => 'Espaco',
            'usuario_ID' => 'Usuario ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHfCaminhos()
    {
        return $this->hasMany(HfCaminho::className(), ['carona_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPontos()
    {
        return $this->hasMany(HfPontos::className(), ['ID' => 'pontos_ID'])->viaTable('hf_caminho', ['carona_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(HfUsuario::className(), ['ID' => 'usuario_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHfQuerers()
    {
        return $this->hasMany(HfQuerer::className(), ['carona_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(HfUsuario::className(), ['ID' => 'usuario_ID'])->viaTable('hf_querer', ['carona_ID' => 'ID']);
    }
}
