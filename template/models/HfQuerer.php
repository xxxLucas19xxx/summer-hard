<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hf_querer".
 *
 * @property int $usuario_ID
 * @property int $carona_ID
 *
 * @property HfUsuario $usuario
 * @property HfCarona $carona
 */
class HfQuerer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hf_querer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_ID', 'carona_ID'], 'required'],
            [['usuario_ID', 'carona_ID'], 'integer'],
            [['usuario_ID', 'carona_ID'], 'unique', 'targetAttribute' => ['usuario_ID', 'carona_ID']],
            [['usuario_ID'], 'exist', 'skipOnError' => true, 'targetClass' => HfUsuario::className(), 'targetAttribute' => ['usuario_ID' => 'ID']],
            [['carona_ID'], 'exist', 'skipOnError' => true, 'targetClass' => HfCarona::className(), 'targetAttribute' => ['carona_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'usuario_ID' => 'Usuário',
            'carona_ID' => 'Carona',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(HfUsuario::className(), ['ID' => 'usuario_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarona()
    {
        return $this->hasOne(HfCarona::className(), ['ID' => 'carona_ID']);
    }
}
