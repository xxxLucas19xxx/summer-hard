<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hf_caminho".
 *
 * @property int $carona_ID
 * @property int $pontos_ID
 *
 * @property HfCarona $carona
 * @property HfPontos $pontos
 */
class HfCaminho extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hf_caminho';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['carona_ID', 'pontos_ID'], 'required'],
            [['carona_ID', 'pontos_ID'], 'integer'],
            [['carona_ID', 'pontos_ID'], 'unique', 'targetAttribute' => ['carona_ID', 'pontos_ID']],
            [['carona_ID'], 'exist', 'skipOnError' => true, 'targetClass' => HfCarona::className(), 'targetAttribute' => ['carona_ID' => 'ID']],
            [['pontos_ID'], 'exist', 'skipOnError' => true, 'targetClass' => HfPontos::className(), 'targetAttribute' => ['pontos_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'carona_ID' => 'Carona',
            'pontos_ID' => 'Pontos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarona()
    {
        return $this->hasOne(HfCarona::className(), ['ID' => 'carona_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPontos()
    {
        return $this->hasOne(HfPontos::className(), ['ID' => 'pontos_ID']);
    }
}
