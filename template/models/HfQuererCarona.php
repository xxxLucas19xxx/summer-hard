<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HfCarona;

/**
 * HfQuererCarona represents the model behind the search form of `app\models\HfCarona`.
 */
class HfQuererCarona extends HfCarona
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'repeticao', 'espaco', 'usuario_ID'], 'integer'],
            [['data', 'horario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HfCarona::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'repeticao' => $this->repeticao,
            'data' => $this->data,
            'horario' => $this->horario,
            'espaco' => $this->espaco,
            'usuario_ID' => $this->usuario_ID,
        ]);

        return $dataProvider;
    }
}
