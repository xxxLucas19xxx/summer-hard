<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hf_usuario".
 *
 * @property int $ID
 * @property string $nome
 * @property string $senha
 * @property string $email
 *
 * @property HfCarona[] $hfCaronas
 * @property HfQuerer[] $hfQuerers
 * @property HfCarona[] $caronas
 */
class HfUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hf_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 30],
            [['senha'], 'string', 'max' => 12],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'Número',
            'nome' => 'Nome',
            'senha' => 'Senha',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHfCaronas()
    {
        return $this->hasMany(HfCarona::className(), ['usuario_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHfQuerers()
    {
        return $this->hasMany(HfQuerer::className(), ['usuario_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaronas()
    {
        return $this->hasMany(HfCarona::className(), ['ID' => 'carona_ID'])->viaTable('hf_querer', ['usuario_ID' => 'ID']);
    }
}
